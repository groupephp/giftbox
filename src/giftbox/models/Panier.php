<?php
namespace giftbox\models;

class Panier extends \Illuminate\Database\Eloquent\Model{

	protected $table ='coffret';
	protected $primaryKey ='id';
	public $timestamps = false;

	public function Panier(){
		return $this->hasMany('\giftbox\models\coffret','idPanier');
	}
	
}