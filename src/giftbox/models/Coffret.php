<?php
namespace giftbox\models;

class Coffret extends \Illuminate\Database\Eloquent\Model{

	protected $table ='compocoffret';
	protected $primaryKey ='idCoffret ,idPrest';
	public $timestamps = false;
	
	public function Panier(){
		return $this->belongsTo('\giftbox\models\Panier','idPanier');
	}

	public function Prestation(){
		return $this->belongsTo('\giftbox\models\Prestation','idPrestation');
	}
}