<?php
namespace giftbox\models;

class Categorie extends \Illuminate\Database\Eloquent\Model{

	protected $table ='categorie';
	protected $primaryKey ='id';
	public $timestamps = false;

	public function prestation(){
		return $this->hasMany('\giftbox\models\Prestation','cat_id');
	}
	
}