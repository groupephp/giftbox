<?php
namespace giftbox\controleur;
use \giftbox\models\Prestation as Prestation;
use \giftbox\models\Categorie as Categorie;
use \giftbox\vues\VueAccueil as VueAccueil;
use \giftbox\vues\VueConnexion as VueConnexion;

class ControleurAccueil{
	
	public function prestation($pres = null){
		if (!is_null($pres)){
			$q = Prestation::where('id','=',$pres)->first();
			$v=new VueAccueil($q);
			print $v->render(2);
		}else{
			$list = Prestation::get();
			$v=new VueAccueil($list);
			$res =$v->render(1);
			print $res;
		}
	}
}