<?php
namespace giftbox\controleur;
use \giftbox\models\Prestation as Prestation;
use \giftbox\models\Categorie as Categorie;
use \giftbox\models\Utilisateur as Utilisateur;
use \giftbox\vues\VueCatalogue as VueCatalogue;
use \giftbox\vues\VueConnexion as VueConnexion;

class ControleurCatalogue{
	
	public function prestation($ordre =null){
		if (!is_null($ordre)){
			if ($ordre=='PrixCroix'){
			$q = Prestation::orderBy('prix')->get();
		}elseif($ordre=='PrixDesc'){
			$q = Prestation::orderBy('prix','DESC')->get();
		}
		$v=new VueCatalogue($q);
		}else{
			$list = Prestation::get();
			$v=new VueCatalogue($list);	
		}
		print $v->render(1);
	}

	public function categorieParPrestation($id = null){
		if (!is_null($id)){
			$q1 = Categorie::get();
			$v=new VueCatalogue($q1);
			print $v->render(3,$id);
		}else{
			$q1 = Categorie::get();
			$v=new VueCatalogue($q1);
			print $v->render(4);
		}
		
	}

	public function rechercheAvancer($nom=null,$prixMin=null,$prixMax=null){
		if($nom==null && $prixMin==null && $prixMax==null){
			$q= array();
			$v = new VueCatalogue($q);
			print $v->render(5);
		}else{
			
			if($prixMin!=null){

				$pi = filter_var($prixMin,FILTER_SANITIZE_NUMBER_FLOAT);
			}else{$pi=0;}
			if($prixMax!=null){
				$pa = filter_var($prixMax,FILTER_SANITIZE_NUMBER_FLOAT);
			}else{$pa=99999;}
			if($nom != null){
				$n = filter_var($nom,FILTER_SANITIZE_STRING);
				$q = Prestation::whereRaw("nom like ? and prix>=? and prix <=?",array('%'.$n.'%',$pi,$pa))->get();
			}else{
				$q = Prestation::whereRaw("prix>=? and prix <=?",array($pi,$pa))->get();
			}
			$v = new VueCatalogue($q);
			print $v->render(1);
		}


	}


}