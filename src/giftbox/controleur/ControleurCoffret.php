<?php
namespace giftbox\controleur;
use \giftbox\models\Prestation;
use \giftbox\models\Categorie;
use \giftbox\models\Coffret;
use \giftbox\vues\VueCatalogue;
use \giftbox\vues\VueCoffret;
use \giftbox\controler\ControlerCatalogue;

class ControleurCoffret{
	
	
	 public function ajouterCoffret($id){
		if(Prestation::find($id) != null){
			//test si l'id existe
			if(isset($_SESSION['coffret'])){
				//si coffret existe
				if(array_key_exists($id, $_SESSION['coffret'])){
					//incrémenter la presta existante
					
					$_SESSION['coffret'][$id]++;
				}else{
					//créer nouvelle presta
					$_SESSION['coffret'][$id] = 1;
				}
			}else{
				//si coffret n'existe pas
				$_SESSION['coffret'][$id] = 1;
			}
		}
		$v=new VueCoffret();
		$res =$v->render(1);
		print $res;
	 }
	 


		public function suppCoffret($id) {
		session_start();
		DBConnection::getInstance();
		if(Prestation::find($id) != null){
			//test si l'id existe
			if(isset($_SESSION['coffret'])){
				//si coffret existe
				if($_SESSION['coffret'][$id]>1){
					$_SESSION['coffret'][$id]--;
				}else{
					//sinon
					$newtab=array();
					foreach($_SESSION['coffret'] as $key=>$value){
						if($key!=$id){
							$newtab[$key]=$value;
						}
					}
					$_SESSION['coffret']= $newtab;
				}
			}
		}
	}
	
	 //public function suppCoffret($id){
	//	if(!isset($_SESSION)){
		//	session_start();
		//}
		//if (!isset($_SESSION['panier'])){ 
		//	$_SESSION['panier']=array();
	//	}
		//unset($_SESSION['panier'][$id]);
		//var_dump($_SESSION['panier']);
	//}
		
	public function afficherPanier() {
		$list = array();
		$total = 0;
		if (!isset($_SESSION['coffret'])){
			$vue= new VueCoffret();
			$vue->render(3);
		}else{
			foreach($_SESSION['coffret'] as $key=>$value){
				$prest =Prestation::find($key);
				$list[] = [$prest,$value];					
				$total+= $prest['prix']*$value;
				$prest['quantite']=$value;
			}

            $vue=new VueCoffret($list,$total);
			print $vue->render(2);
			}
			
		}
		
		
		
	
	
	public function validationCoffret() {
		//on verifie que le coffret existe et a plus de 2 articles
		$vue=new VueCoffret();
		if(isset($_SESSION['pseudo'])){
			if(isset($_SESSION['coffret']) && count($_SESSION['coffret']) >= 2){
				$i=0;
				$idCatg='';
				foreach($_SESSION['coffret'] as $key=>$value){
					$list_presta[] = Prestation::find($key);
					$qte[]=$value;
					if($idCatg!=Prestation::select('cat_id')->find($key)){
						//verification des 2 categories
						$idCatg=Prestation::select('cat_id')->find($key);
						$i++;
					}
				}
			if($i>=2){
				//verification des > 2 articles
				//faire le recap des prestation dans la vue
						if(isset($_POST['message'])){
							$message=$_POST['message'];
						}
						$type=$_POST['paiement'];
						$coffret=new Coffret();
						$coffret->etat='impayé';
						if(isset($_POST['message'])){
							$coffret->msgCli=$message;
						}
						$coffret->typePaie=$type;
						$coffret->save();
						$id=$coffret['id'];
						$list_presta=array();
						foreach($_SESSION['coffret'] as $key=>$value){
							$panier = new Panier();
							$panier->idCoffret=$id;
							$panier->idPrest=$key;
							$panier->quantite=$value;
							$panier->save();
							$list_presta[] = Prestation::find($key);
						}
						$vue=new VueCoffret($list_presta, $_SESSION['coffret']);
						print $vue->render(5);
						session_destroy();
					}
				else{
					//article < 2 categories
					$vue=new VueCoffret();
					print $vue->render(4);
				}
			}
			else{
				//moins de 2 articles dans le panier
				$vue=new VueCoffret();
				print $vue->render(4);
			}
		}
		else{
			//utilisateur pas co
			$vue=new VueCoffret();
			print $vue->render(6);
		}
	}
		
	
		
		
	
	
	public function finalisation() {
		DBConnection::getInstance();
		$coffret=Coffret::find($id);
		$coffret->statut='payé';
		$coffret->save();
		$vue=new VueCoffret();
		print $vue->render(5);
	}
}