<?php
namespace giftbox\controleur;
use \giftbox\models\Utilisateur as Utilisateur;
use \giftbox\vues\VueConnexion as VueConnexion;

class ControleurConnexion{
	private $url;
	public function __construct($u){
		$this->url = $u;
	}
	public function connexion($pseudo =null,$mp=null){
		// affiche les prestations avec la note la plus haute
		// controle la connexion
		$connect = new VueConnexion($this->url);
		if(!isset($_SESSION['pseudo'],$_SESSION['mp'])){
			if (!isset($pseudo)){
				$html= $connect->render(1);
			}else{
				//filtrer pseudo et mot de passe ici !!!!
				$pseudo=filter_var($pseudo,FILTER_SANITIZE_STRING);
				$mp=filter_var($mp,FILTER_SANITIZE_STRING);
				$verif = Utilisateur::where('pseudo', '=',$pseudo)->first();
				if (empty($verif['pseudo']) || /*!password_verify($mp,$verif['mp'])*/ $mp!=$verif['mp']){
					$html= $connect->render(2);
				}else{
					$_SESSION['pseudo'] = $pseudo;
					$_SESSION['mp'] = $mp;
					$html = $connect->render(3);
				}
			}
	}else{
		$html = $connect->render(3);
	}
		print $html;
	}

}