<?php

namespace giftbox\vues;

class VueCatalogue {
	
	private $listObj;
	
	public function __construct($list){
		$this->listObj= $list;
	}
	
	private function list_presta() {
		$html='<tbody> ';
		$app = \Slim\Slim::getInstance();
		$html.='<form id="AjoutPanier"
				method="GET" action="'.$app->urlFor('AjoutCoffret').'">';
		foreach($this->listObj as $presta){
			$html.='<tr><td>'.$presta['nom'].'</td>'.'<td>'.$presta['descr'].'</td>'.'<td>'.$presta['prix'].'</td>'.'<td>'.'<img src="img/'.$presta['img'].'" width=50 height=50 ><td>
		<button type="submit" name="id" value="'.$presta['id'].'"> Ajouter au panier </button></td></tr>';
		}
		$html.='</form></tbody>';

		return $html;
	}
	
	private function list_categ() {
		
		$app = \Slim\Slim::getInstance();
		$html='<tbody> ';
		$html.='<form id="AjoutPanier"
				method="GET" action="'.$app->urlFor('AjoutCoffret').'">';
		foreach($this->listObj as $categ){
			$html.='<tr><td><strong>'.$categ['nom'].'<strong></td></br>';
		$listepres=$categ->prestation()->get();
			foreach($listepres as $presta){
				$html.='<tr><td>'.$presta['nom'].'</td><td>'.$presta['descr'].'</td><td>'.$presta['prix'].'</td><td>'.'<img src="img/'.$presta['img'].'" width=50 height=50 ></td><td><button type="submit" name="id" value="'.$presta['id'].'"> Ajouter au panier </button></td></tr>';
			}
			
		}
		$html.='</tbody>';
		return $html;
	}

	/*private function list_prestacat($id=null) {
		$app = \Slim\Slim::getInstance();
		$html='<tbody>';
		$html.='<form id="AjoutPanier"
				method="POST" action="coffret/add">';
		//$html='<section> <form method="POST" action='.$app->urlFor('cataloguePOST').'>
		//			<p> ';

		foreach($this->listObj as $categ){
			//if($categ['id']==$id){
			$html.='<strong>'.$categ['nom'].'<strong>';//<button type="submit" name="cat" value=-1>réduire détail</button></br>';
			$listepres=$categ->prestation()->get();
			
			foreach($listepres as $presta){
				$html.='<tr><td>'.$presta['nom'].'</td><td>'.$presta['descr'].'</td><td>'.$presta['prix'].'</td><td><img src="/giftbox/img/'.$presta['img'].'" width=50 height=50 ></tr>';
			}
			
		//}else{
			//$html.=$categ['nom'].'<button type="submit" name="cat" value="'.$categ['id'].'">détailler les prestations</button></br>';
		//}
		}
		//$html.='</p></form></section>';
		$html.='</tbody>';
		return $html;
	}

	//private function list_prestacat() {
		//$html='<section><table>';
		//foreach($this->listObj as $categ){
			//$html.=$categ['nom'].'</br>';
			//$listepres=$categ->prestation()->get();
			//foreach($listepres as $presta){
			//	$html.='<tr><td>'.$presta['nom'].'</td><td>'.$presta['descr'].'</td><td>'.$presta['prix'];
			//	$html.='<input type="button" value="Ajouter au panier">';
			//}
		//}
		//$html.='</table></section>';

		//return $html;
	//}
	
	private function list_prestaindiv() {


		$html='<tbody>';
		$html.='<tr><td>'.$this->listObj['nom'].'</td></tr>';
		$html.='<td><input type="button" value="Ajouter au panier" onclick="ajouterCoffret($this->listObj)"></td></table></section>';
		$html.='</tbody>';

		return $html;
	}
	*/
	
	public function render($i,$id=null) {
		switch ($i) {
			case 1 : {
				$content = '<div class="col-md-6">
         		 <table class="table table-bordered">
           			<thead>
             		 <tr>
               		 	<th>Nom de la prestation </th>
                			<th>Description</th>
                			<th>Prix</th>
               			 	<th>Image</th>
             			 </tr>
           			 </thead>'.$this->list_presta().
           			  '</table></div>';
           			 
				break;
			}
			case 2 : {
				$content = 
				'<div class="col-md-6">
         		 <table class="table table-bordered">
           			<thead>
             		 <tr>
               		 	<th>Nom de la prestation </th>
               		 	<th>Description</th>
                			<th>Prix</th>
                			
               			 	<th>Image</th>
             			 </tr>
           			 </thead>'.$this->list_prestaindiv().'</table></div>';
				break;
			}
			case 3: {

				$content = 
				'<div class="col-md-6">
         		 <table class="table table-bordered">
           			<thead>
             		 <tr>
               		 	<th>Nom de la prestation </th>
               		 	<th>Description</th>
                			<th>Prix</th>              			
               			 	<th>Image</th>
             			 </tr>
           			 </thead>'.$this->list_prestacat($id).'</table></div>';



				break;
			}
			case 4: {
				$content = 
				'<div class="col-md-6">
         		 <table class="table table-bordered">
           			<thead>
             		 <tr>
             		 	
               		 	<th>Catégorie / Nom de la prestation </th>
               		 	<th>Description</th>
                			<th>Prix</th>               			
               			 	<th>Image</th>
             			 </tr>
           			 </thead>'.$this->list_categ().'</table></div>';
				break;
			}
		}
			$app = \Slim\Slim::getInstance();
		$u = $app->urlFor('cataloguePOST');
		$html = <<<END
Catalogue de prestation :</br>
<form id="selectionDuCatalogue" method="POST" action="$u">
<button type="submit" name="Affichage" value="categorie">Catégorie</button>
<button type="submit" name="Affichage" value="prestation">Prestation</button>
Prix croissant <input type="radio" name="TypePrestation" value="PrixCroix" checked>
Prix décroissant <input type="radio" name="TypePrestation" value="PrixDesc"></br>
Nom de la prestation :<input type="texte" name="nom"/> Prix minimum : <input type="number" name="PrixMin"/> Prix maximum : <input type="number" name="PrixMax"/>  
<button type="submit" name="Affichage" value="recherche avancer d'une prestation">Recherche avancé d'une prestation</button>
</form>
<div class="container">
		$content
 </div>   </div>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="ressource/Bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>
END;

		
		return $html;
	}
		
		
		
}
