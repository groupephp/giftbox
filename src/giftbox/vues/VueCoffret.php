<?php

namespace giftbox\vues;

class VueCoffret {

	private $listObj,$total;

	
	public function __construct($list=null,$t=null){
		$this->listObj= $list;
		$this->total=$t;
	}
	
	//Fonction qui permet un choix entre continuer Achats et Payer
	private function ajout_prest(){
		$app = \Slim\Slim::getInstance();
		$html='<div class="well"><section>';
		$html.='<p>Vous avez ajouter une prestation à votre panier !</p>';
		$html.='<form id="ContinueAchat"
				method="GET" action="'.$app->urlFor('catalogueGET').'">';
		$html.='<input type="submit" name="continue" value="Continuer vos achats"></form>';
		$html.='<form id="PayerCommande"
				method="GET" action="'.$app->urlFor('validePOST').'">';
		$html.='<input type="submit" value="Payer votre commande"></form></section></div>';
		return $html;
		
	}
	
	
	//Fonction qui affiche le panier
	private function afficher_Panier(){
		$app = \Slim\Slim::getInstance();
		
		$html='<section>';
		$html.='<p> Votre panier contient actuellement : </br>';
		foreach ($this->listObj as $prest) {
			$html.=$prest[0]['nom'].' prix:  '.$prest[0]['prix'].'  qte:   '.$prest[0]['quantite'].'</br>';
		}
		$html.='Total TTC ='.$this->total.'</p></br>';
		$html.='<input type="submit" name="valid" value="Valider votre panier">';
		return $html;
	}
	
	//Fonction pour affichage du formulaire de paiement
	private function valider_Panier(){
		$app = \Slim\Slim::getInstance();
		$html='<section> 
		Merci d\'avoir validé votre panier, maintenant il ne vous reste plus qu\'à choisir le mode de paiement et le message qui accompagnera votre coffret cadeau.</br>';
		$html.='<form id="final panier" method="GET">
			<div style="font-size:18px;">
							<div style="width:130px; display:inline-block;">Votre nom : </div><input type="text" name="nom" required/><br />
							<div style="width:130px; display:inline-block;">Votre prénom : </div><input type="text" name="prenom" required/><br />
							<div style="width:130px; display:inline-block;">Votre email : </div><input type="email" name="email" required/><br />
							<div style="width:130px; display:inline-block;">Votre message : </div><textarea name="message" cols="50" rows="10" required></textarea><br />
							Mode de paiement : 
							<input type="radio" name="modePaiement" value="classique" id="classique" /> Classique
							<input type="radio" name="modePaiement" value="cagnotte" id="cagnotte" />Cagnote<br /></form></div>';
		
		return $html;

	}
	
	public function render($i) {
		switch ($i) {
			case 1 : {
				$content = '<div class="container">'.$this->ajout_prest().'</div>';
				$r='./../../';
				break;
			}
			case 2 : {
				$content = '<div class="container">'.$this->afficher_Panier().'</div>';
			$r='./../../';
				break;
			}
			case 3 :{
				$content='<p>Panier vide</p>';
				$r='./../../../';
				break;
			}
			case 4:{
				$content='<p> Vous ne pouvez pas valider un coffret si il ne contient pas deux prestations de deux catégories différentes.</br>'.'
								cliquez <a href="'.$app->urlFor('catalogueGET').'">ici</a> pour continuer vos achats';
								$r='./../../';
				break;
			}
			case 5:
				$content= '<div class="container">'.$this->valider_Panier().'</div>';
				$r='./../../../';
				break;
			case 6:
					$content = 'Vous devez être connecter pour pouvoir valider votre panier!';
					$r='./../../';
				break;
		}
		$html = <<<END
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="{$r}ressource/Bootstrap/dist/css/bootstrap-theme.min.css" >
<link rel="stylesheet" href="{$r}ressource/Bootstrap/dist/css/bootstrap.min.css" >
<body>
<div class="container">

		$content
</div> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{$r}ressource/Bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
END;

		
		return $html;
	}	
	

	
}
	
	