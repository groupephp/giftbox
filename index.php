<?php

require ('vendor/autoload.php');
session_start();
use Illuminate\Database\Capsule\Manager as DB;
use \giftbox\controleur\ControleurCatalogue as Catalogue;
use \giftbox\controleur\ControleurConnexion as Connexion;
use \giftbox\controleur\ControleurInscription as Inscription;
use \giftbox\controleur\ControleurAccueil as Accueil;
use \giftbox\controleur\ControleurCoffret as Coffret;
$db = new DB();
$file=parse_ini_file( 'src/conf/conf.ini' );
$db->addConnection( [
	'driver' => $file['db_driver'],
	'host' => $file['host'],
	'database' => $file['dbname'],
	'username' => $file['db_user'],
	'password' => $file['db_password'],
	'charset' => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$app->get('/prest',function(){
	$p=new Catalogue();
	$s = \Slim\Slim::getInstance();
;
	$p->prestation();
});

$app->get('/prest/:id',function($id){
	$p=new Catalogue();
	$p->prestation($id);
});

$app->get('/cat/:id',function($id){
	$p=new Catalogue();
	$p->categorieParPrestation($id);
});
 
$app->get('/cat',function(){
	$s = \Slim\Slim::getInstance();
		if (null !=$s->request->post('deco')){
 	unset($_SESSION['pseudo'],$_SESSION['mp']); //supprimer quand le bouton deconnexion a été appuyer
 }
	$con = new Connexion($s->urlFor('catalogueGET'));
	$cat=new Catalogue();
	$con->connexion();
	if($s->request->post('Affichage') == 'categorie'){
		if($s->request->post('cat')!=null){
			$cat->categorieParPrestation($s->request->post('cat'));
		}else{$cat->categorieParPrestation();}
	}elseif($s->request->post('Affichage')=='recherche avancer d\'une prestation'){
		// à faire
	}else{
		if($s->request->post('TypePrestation')==null){
			$cat->prestation('PrixCroix');
		}else{
			$cat->prestation($s->request->post('TypePrestation'));
		}
	}
})->name('catalogueGET');

/*$app->get('/pres/prix/:ordre',function($ordre){
	$p=new \giftbox\controleur\ControleurNotation();
	$p->prestationPrix($ordre);
})->name('catalogueGET');*/
$app->post('/cat',function(){
	$s = \Slim\Slim::getInstance();
		if (null !=$s->request->post('deco')){
 	unset($_SESSION['pseudo'],$_SESSION['mp']); //supprimer quand le bouton deconnexion a été appuyer
 }
	$con = new Connexion($s->urlFor('cataloguePOST'));
	$cat=new Catalogue();
	$con->connexion($s->request->post('pseudo'),$s->request->post('mp'));
	if($s->request->post('Affichage') == 'categorie'){
		if($s->request->post('cat')!=null){
		$cat->categorieParPrestation($s->request->post('cat'));
		}else{
			$cat->categorieParPrestation();}
	}elseif($s->request->post('Affichage')=='recherche avancer d\'une prestation'){
		
		$cat->rechercheAvancer($s->request->post('nom'),$s->request->post('PrixMin'),$s->request->post('PrixMax'));
	}else{
		if($s->request->post('TypePrestation')==null){
			$cat->prestation('PrixCroix');
		}else{
			$cat->prestation($s->request->post('TypePrestation'));
		}
	}
})->name('cataloguePOST');


$app->get('/pres/prix/:ordre',function($ordre){
	$p=new \giftbox\controleur\ControleurNotation();
	$p->prestationPrix($ordre);
});

$app->get('/pres/note',function(){
	$p=new \giftbox\controleur\ControleurNotation();
	$p->prestationNote();
});
$app->get('/inscription',function(){
	$con = new Inscription();
	$con->inscription(true);
})->name('inscriptionGET');



$app->post('/inscription',function(){
	$s = \Slim\Slim::getInstance();
	$con=new Inscription();
	$con->inscription(false,$s->request->post('pseudo_inscrit'),$s->request->post('mp_inscrit'),$s->request->post('cmp_inscrit'),$s->request->post('email_inscrit'),$s->request->post('cemail_inscrit'));
})->name('inscriptionPOST');



$app->post('/',function(){
	$s = \Slim\Slim::getInstance();
		if (null !=$s->request->post('deco')){
 	unset($_SESSION['pseudo'],$_SESSION['mp']); //supprimer quand le bouton deconnexion a été appuyer
 }
	$con = new Connexion($s->urlFor('accueilPOST'));
	$cat=new Accueil();
	$con->connexion($s->request->post('pseudo'),$s->request->post('mp'));
	$cat->prestation();
})->name('accueilPOST');

$app->get('/',function(){
	$s = \Slim\Slim::getInstance();
	if (null !=$s->request->post('deco')){
 		unset($_SESSION['pseudo'],$_SESSION['mp']); //supprimer quand le bouton deconnexion a été appuyer
 	}
	$con = new Connexion($s->urlFor('accueilGET'));
	$cat=new Accueil();
	$con->connexion();
	$cat->prestation();
})->name('accueilGET');




$app->get('/panier',function(){
	$s = \Slim\Slim::getInstance();
		if (null !=$s->request->post('deco')){
 	unset($_SESSION['pseudo'],$_SESSION['mp']); //supprimer quand le bouton deconnexion a été appuyer
 	}
	$con = new Connexion($s->urlFor('panierGET'));
	$cat=new Accueil();
	$con->connexion($s->request->post('pseudo'),$s->request->post('mp'));
	$p=new Coffret();
	$p->afficherPanier();
})->name('panierGET');

$app->post('/panier',function(){
	$s = \Slim\Slim::getInstance();
		if (null !=$s->request->post('deco')){
 	unset($_SESSION['pseudo'],$_SESSION['mp']); //supprimer quand le bouton deconnexion a été appuyer
 	}
	$con = new Connexion($s->urlFor('panierPOST'));
	$cat=new Accueil();
	$con->connexion($s->request->post('pseudo'),$s->request->post('mp'));
	$p=new Coffret();
	$p->afficherPanier();
})->name('panierPOST');

$app->get('/coffre/add/',function(){
	$s = \Slim\Slim::getInstance();
	$p=new Coffret();
	$p->ajouterCoffret($s->request->get("id"));
})->name('AjoutCoffret');

$app->post('/coffre/add/',function(){
	$s = \Slim\Slim::getInstance();
	$p=new Coffret();
	$p->ajouterCoffret($s->request->get("id"));
})->name('AjoutCoffretPOST');


$app->get('/cadeau',function(){
	$p=new Coffret();
	$p->generationURL();
})->name('urlCadeau');


$app->get('/coffre/vald',function(){
	$s = \Slim\Slim::getInstance();
		if (null !=$s->request->post('deco')){
 	unset($_SESSION['pseudo'],$_SESSION['mp']); //supprimer quand le bouton deconnexion a été appuyer
 	}
	$con = new Connexion($s->urlFor('panierGET'));
	$con->connexion($s->request->post('pseudo'),$s->request->post('mp'));
	$p=new Coffret();
	echo "GET";
	$p->validationCoffret();
})->name('valideGET');




$app->post('/coffre/vald',function(){
	$s = \Slim\Slim::getInstance();
		if (null !=$s->request->post('deco')){
 	unset($_SESSION['pseudo'],$_SESSION['mp']); //supprimer quand le bouton deconnexion a été appuyer
 	}
	$con = new Connexion($s->urlFor('panierPOST'));
	$con->connexion($s->request->post('pseudo'),$s->request->post('mp'));
	echo "POST";
	$p=new Coffret();
	$p->validationCoffret();
})->name('validePOST');




$app->get('/Cadeau/:id',function($mp){
	$p=new \giftbox\controleur\ControleurCoffret();
	$p->ajoutMpCoffret($mp);
});

$app->get('/coffret/supp/:id',function($id){
	$p=new \giftbox\controleur\ControleurCoffret();
	$p->suppCoffret($id);
});

$app->get('/cagnotte',function(){
	$p=new \giftbox\controleur\ControleurCagnotte();
	$p->creaCagnotte();
});

$app->get('/cagnotte/participe/:montant',function($montant){
	$p=new \giftbox\ControleurCagnotte\ControleurCagnotte();
	$p->participCagnotte($montant);
});

$app->get('/cagnotte/cloturer',function(){
	$p=new \giftbox\ControleurCagnotte\ControleurCagnotte();
	$p->closeCagnotte();
});

$app->get('/cadeau',function(){
	$p=new \giftbox\ControleurCadeau\ControleurCadeau();
	$p->urlCadeau();
});

$app->get('/cadeau/ouverture',function(){
	$p=new \giftbox\ControleurCadeau\ControleurCadeau();
	$p->ouvertureCadeau();
});

$app->get('/gestion/add/:id',function($id){
	$p=new \giftbox\ControleurGestion\ControleurGestion();
	$p->addPrestation($id);
});

$app->get('/gestion/supp/:id',function($id){
	$p=new \giftbox\ControleurGestion\ControleurGestion();
	$p->suppPrestation($id);
});

$app->get('/gestion/desact/:id',function($id){
	$p=new \giftbox\ControleurGestion\ControleurGestion();
	$p->desacPrestation($id);
});

$app->get('/authen',function(){
	$p=new \giftbox\ControleurAuten\ControleurAuten();
	$p->authen();
});

$app->run();





